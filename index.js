let text = "hello";

function fillElement(text) {
  const newSpan = document.createElement("span");
  newSpan.innerText = text;
  document.body.appendChild(newSpan);
  return newSpan;
}

fillElement(text);
